## Business application
![Java](https://img.shields.io/badge/Java-1.8-brightgreen.svg)
![Springboot](https://img.shields.io/badge/SpringBoot-2.0.4.RELEASE-brightgreen.svg)
![Thymeleaf](https://img.shields.io/badge/Thymeleaf-OK-brightgreen.svg)
![jQuery](https://img.shields.io/badge/jQuery-3.3.1-brightgreen.svg)
![Maven](https://img.shields.io/badge/Maven-OK-brightgreen.svg)

### Small business aplication made within two days for a company test position as Java Junior Developer

### To run the test use one the commands bellow:
#### First get inside the business application folder

java -jar bin/business-0.0.1-SNAPSHOT.jar

- In the browser type http://localhost:8080 and use the system.