

function getPrice(element) {
	
	var priceField = document.getElementById("price");
	var productSelection = element.options[element.selectedIndex].value;
	var price;
	var url = "http://localhost:9000/products/price?id="+productSelection;
		
	$.ajax({
		url: url,
		type: "get",
	})
	.done(function(data){
		priceField.value = data;
	})
	getPaymentOptions(element);
}

function getPaymentOptions(element) {
	
	var optionField = document.getElementById("options");
	var productSelection = element.options[element.selectedIndex].value;
	var url = "http://localhost:9000//products/payment?id="+productSelection;
	var optionArray;
	var options = "";
		
	$.ajax({
		url: url,
		type: "get",
	})
	.done(function(data){
		console.log(data);

		for(var i = 0; i < data.length; i++) {
			console.log(data);
			options += "<option value='"+data[i]+"'>"+data[i]+"</option>";
		}
		optionField.innerHTML = options;
	})
	
}