package engSoft.business.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import engSoft.business.service.ProductsService;

@Controller
public class ProductsController {
	@Autowired
	private ProductsService service;
	
	@GetMapping("/")
	public ModelAndView getAllProduct() throws Exception {
		ModelAndView mv = new ModelAndView("/product");
		mv.addObject("products", service.getAllProducts());
		return mv;
	}
	
	@GetMapping("/products/price")
	@ResponseBody
	public ResponseEntity<Double> getPriceByProduct(@RequestParam String id) throws Exception {
		return new ResponseEntity<Double>(service.getPriceByProduct(id), HttpStatus.OK);
	}
	
	@GetMapping("/products/payment")
	@ResponseBody
	public ResponseEntity<List <String>> getPaymentOptionsByProduct(@RequestParam String id) throws Exception {
		return new ResponseEntity<List <String>>(service.getPaymentOptionsByProduct(id), HttpStatus.OK);
	}
}
