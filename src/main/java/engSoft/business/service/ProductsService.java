package engSoft.business.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import engSoft.business.model.Product;

@Service
public class ProductsService {
	@Autowired
	XmlService xmlService;	
	public List<Product> getAllProducts() throws Exception {
		return xmlService.getProductsId();
	}

	public double getPriceByProduct(String id) throws Exception {
		return xmlService.getProductPrice(id);
	}

	public List <String> getPaymentOptionsByProduct(String id) throws Exception {
		return xmlService.getProductPaymentOptions(id);
	}
	
}
