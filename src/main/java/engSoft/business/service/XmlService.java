package engSoft.business.service;

import java.util.List;

import org.springframework.stereotype.Service;

import engSoft.business.model.Product;
@Service
public class XmlService {
	
	public List <Product> getProductsId() throws Exception {
		XmlReader xml = new XmlReader();
		return xml.xmlReadList();
	}

	public double getProductPrice(String id) throws Exception {
		XmlReader xml = new XmlReader();
		return xml.xmlReadItem(id).getPrice();
	}

	public List <String> getProductPaymentOptions(String id) throws Exception {
		XmlReader xml = new XmlReader();
		return  xml.xmlReadItem(id).getPayment();
	}

}
