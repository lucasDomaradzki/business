package engSoft.business.service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import engSoft.business.exception.ResourceNotFoundException;
import engSoft.business.model.Product;

public class XmlReader {

	private NodeList xmlProductList;
	private static String ATTRIBUTE_ID = "id";
	private static String ATTRIBUTE_PRICE = "price";
	private static String ATTRIBUTE_PAYMENT = "payment";
	private static String ATTRIBUTE_PAYOPTIONS = "options";

	public XmlReader() throws Exception {
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(new File("src/main/resources/templates/product.xml"));

			doc.getDocumentElement().normalize();

			xmlProductList = doc.getElementsByTagName("product");

		} catch (Exception e) {
			throw new ResourceNotFoundException("Arquivo .xml não encontrado.");
		}
	}

	public List<Product> xmlReadList() throws Exception {

		List<Product> products = new ArrayList<>();

		for (int i = 0; i < xmlProductList.getLength(); i++) {
			Node productSpecs = xmlProductList.item(i);

			if (productSpecs.getNodeType() == Node.ELEMENT_NODE) {

				Element eElement = (Element) productSpecs;
				String productId = eElement.getElementsByTagName(ATTRIBUTE_ID).item(0).getTextContent();
				
				Product product = new Product();				
				product.setId(productId);
				products.add(product);
			}
		}
		return products;
	}

	public Product xmlReadItem(String id) throws Exception {
		Product product = new Product();
		for (int i = 0; i < xmlProductList.getLength(); i++) {
			Node result = xmlProductList.item(i);

			if (result.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) result;
				String productId = eElement.getElementsByTagName(ATTRIBUTE_ID).item(0).getTextContent();
				if (id.equals(productId)) {
					product.setId(productId);
					String priceXml = eElement.getElementsByTagName(ATTRIBUTE_PRICE).item(0).getTextContent();
					product.setPrice(Double.parseDouble(priceXml));
					
					NodeList childNodes = eElement.getElementsByTagName(ATTRIBUTE_PAYMENT).item(0).getChildNodes();
					
					List <String> payments = new ArrayList<>();
					for(int j = 0; j < childNodes.getLength(); j++) {
						if (childNodes.item(j).getNodeType() == Node.ELEMENT_NODE) {
							payments.add(childNodes.item(j).getTextContent());
						}					
						
					}
					product.setPayment(payments);
					
				}
			}
		}
		return product;
	}

}
