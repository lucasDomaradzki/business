package engSoft.business.model;

import java.util.List;

public class Product {
	
	private String id;
	private double price;
	private List<String> payment;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public List<String> getPayment() {
		return payment;
	}

	public void setPayment(List<String> payment) {
		this.payment = payment;
	}
	
}
